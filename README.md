# TP RESTFUL d'Architectures Distribuees (TP5 & TP6)

</p>
HAZARD Alexandre<br/>
LE CREURER Benjamin 
</p>

![alt text](https://gitlab.com/Eddenn/TP-AD-EIP/raw/master/schema.png)

<table>
<tbody>
<tr>
<td width="85">
<p><strong>M&Eacute;THODE</strong></p>
</td>
<td width="151">
<p><strong>URL</strong></p>
</td>
<td width="57">
<p><strong>BODY</strong></p>
</td>
<td width="311">
<p><strong>DESCRIPTION</strong></p>
</td>
</tr>

<tr>
<td width="85">
<p>GET</p>
</td>
<td width="151">
<p>/animals</p>
</td>
<td width="57">
<p>&nbsp;</p>
</td>
<td width="311">
<p>Retourne l'ensemble des animaux du centre</p>
</td>
</tr>

<tr>
<td width="85">
<p>GET</p>
</td>
<td width="151">
<p>/animals/{animal_id}</p>
</td>
<td width="57">
<p>&nbsp;</p>
</td>
<td width="311">
<p>Retourne l&rsquo;animal identifi&eacute; par {animal_id}</p>
</td>
</tr>

<tr>
<td width="85">
<p>&nbsp;</p>
</td>
<td width="151">
<p>&nbsp;</p>
</td>
<td width="57">
<p>&nbsp;</p>
</td>
<td width="311">
<p>&nbsp;</p>
</td>
</tr>

<tr>
<td width="85">
<p>POST</p>
</td>
<td width="151">
<p>/animals</p>
</td>
<td width="57">
<p>Animal</p>
</td>
<td width="311">
<p>Ajoute un animal dans votre centre</p>
</td>
</tr>

<tr>
<td width="85">
<p>GET</p>
</td>
<td width="151">
<p>/find/byName/{name}</p>
</td>
<td width="57">
<p>&nbsp;</p>
</td>
<td width="311">
<p>Recherche d'un animal par son nom</p>
</td>
</tr>

</tbody>
</table>