package tp;

import java.io.StringWriter;
import java.util.UUID;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.log4j.BasicConfigurator;

import tp.model.Animal;

public class ProducerConsumerRestful {
	
	private static final String SERVICE1_URL = "http://localhost:8080/";
	private static final String SERVICE2_URL = "http://localhost:8081/";
	private static final String SERVICE3_URL = "http://localhost:8082/";

    public static void main(String[] args) throws Exception {
        // Configure le logger par défaut
        BasicConfigurator.configure();
        // Contexte Camel par défaut
        CamelContext context = new DefaultCamelContext();
        // Crée une route contenant le consommateur
        RouteBuilder routeBuilder = new RouteBuilder() {
            @Override   	
            public void configure() throws Exception {
            	from("direct:GETAnimals")
            		.setHeader("Accept",constant("application/xml"))
	            	.setHeader(Exchange.HTTP_METHOD,constant("GET"))
	            	.setHeader(Exchange.HTTP_PATH,simple("animals"))
	            	.multicast(new CenterAggregationStrategy()).parallelProcessing()
		            	.enrich(SERVICE1_URL)
		            	.enrich(SERVICE2_URL)
		            	.enrich(SERVICE3_URL)
		            	.end()
	            	.log("reponse received :\n ${body}");

	            	
            	from("direct:ADDAnimal")
	        		.setHeader("Accept",constant("application/xml"))
	            	.setHeader(Exchange.HTTP_METHOD,constant("POST"))
	            	.setHeader(Exchange.HTTP_PATH,simple("animals"))
	            	.setHeader(Exchange.CONTENT_TYPE, constant("application/xml"))
	            	.setBody(simple("${property.animal}"))
		            .multicast(new AnimalAggregationStrategy()).parallelProcessing()
			           	.enrich(SERVICE1_URL)
			           	.enrich(SERVICE2_URL)
			           	.enrich(SERVICE3_URL)
			           	.end()   
	            	.log("reponse received :\n ${body}");
            	
            	from("direct:FindById")
            	.setHeader("Accept",constant("application/xml"))
            	.setHeader(Exchange.HTTP_METHOD,constant("GET"))
            	.setHeader(Exchange.HTTP_PATH,simple("/animals/${property.id}"))
	            .multicast(new AnimalAggregationStrategy()).parallelProcessing()
		           	.enrich(SERVICE1_URL)
		           	.enrich(SERVICE2_URL)
		           	.enrich(SERVICE3_URL)
		           	.end()    
            	.log("reponse received :\n ${body}");
            	
            	from("direct:FindByName")
	            	.setHeader("Accept",constant("application/xml"))
	            	.setHeader(Exchange.HTTP_METHOD,constant("GET"))
	            	.setHeader(Exchange.HTTP_PATH,simple("find/byName/${property.name}"))
		            .multicast(new AnimalAggregationStrategy()).parallelProcessing()
			           	.enrich(SERVICE1_URL)
			           	.enrich(SERVICE2_URL)
			           	.enrich(SERVICE3_URL)
			           	.end()    
	            	.log("reponse received :\n ${body}");

            	
            	from("direct:GeoNames")
            		.setHeader("Accept",constant("application/xml"))
	            	.setHeader(Exchange.HTTP_METHOD,constant("GET"))
	            	.setHeader(Exchange.HTTP_QUERY, simple("q=${property.q}&maxRows=1&username=m1gil"))
	            	.to("http://api.geonames.org/search")
	            	.log("reponse received :\n ${body}");
            }
        };
        // On ajoute la route au contexte
        routeBuilder.addRoutesToCamelContext(context);
        // On démarre le contexte pour activer les routes
        context.start();
        // On crée un producteur
        ProducerTemplate pt = context.createProducerTemplate();
        
        // Envoi vers la route direct:GETAnimals
        pt.sendBody("direct:GETAnimals", "");
        
        // Envoi vers la route direct:FindByName avec la propriété "name"
        pt.sendBodyAndProperty("direct:FindByName", "","name","Canine");
        
        // Envoi vers la route direct:GeoNames avec la propriété "q"
        pt.sendBodyAndProperty("direct:GeoNames", "","q","Biotropica");
        
        // Envoi vers la route direct:ADDAnimal avec la propriété "aniaml"
        Animal a = new Animal("Roo", "usa", "Chien", UUID.randomUUID());
        pt.sendBodyAndProperty("direct:ADDAnimal", "","animal", marshal(a));
        
        // Envoi vers la route direct:ADDAnimal avec la propriété "aniaml"
        pt.sendBodyAndProperty("direct:FindById", "","id",a.getId().toString());
        
    }
    
	private static String marshal(Animal animal) throws JAXBException {
	    StringWriter stringWriter = new StringWriter();

	    JAXBContext jaxbContext = JAXBContext.newInstance(Animal.class);
	    Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

	    jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,
	        true);

	    QName qName = new QName("", "animal");
	    JAXBElement<Animal> root = new JAXBElement<Animal>(qName, Animal.class, animal);

	    jaxbMarshaller.marshal(root, stringWriter);

	    return stringWriter.toString();
	}
}
