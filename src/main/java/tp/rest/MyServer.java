package tp.rest;

import java.io.StringWriter;
import java.util.UUID;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;

import org.apache.camel.CamelContext;
import org.apache.camel.CamelExecutionException;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.log4j.BasicConfigurator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

import tp.AnimalAggregationStrategy;
import tp.CenterAggregationStrategy;
import tp.model.*;

import static org.springframework.http.MediaType.*;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@EnableAutoConfiguration
public class MyServer {

	/**
	 * Méthode lançeant le serveur de routage
	 * @param args
	 * @throws Exception
	 */
    public static void main(String[] args) throws Exception {
    	BasicConfigurator.configure();
    	
    	//SpringApplication.run(MyServiceController.class, args);
    	
        System.getProperties().put( "server.port", "8080" );   
        SpringApplication.run(MyServer.class); 
    }
    
    ProducerTemplate producerTemplate;

	private static final String SERVICE1_URL = "http://localhost:8081/";
	private static final String SERVICE2_URL = "http://localhost:8082/";
	private static final String SERVICE3_URL = "http://localhost:8083/";
	
	/**
	 * Constructeur initialisant les routes, producteurs et consommateurs
	 */
    public MyServer() {
	    // Configure le logger par défaut
	    BasicConfigurator.configure();
	    // Contexte Camel par défaut
	    CamelContext context = new DefaultCamelContext();
	    // Crée une route contenant le consommateur
	    RouteBuilder routeBuilder = new RouteBuilder() {
	        @Override   	
	        public void configure() throws Exception {
	        	from("direct:GETAnimals")
	        		.setHeader("Accept",constant("application/xml"))
	            	.setHeader(Exchange.HTTP_METHOD,constant("GET"))
	            	.setHeader(Exchange.HTTP_PATH,simple("animals"))
	            	.multicast(new CenterAggregationStrategy()).parallelProcessing()
		            	.enrich(SERVICE1_URL)
		            	.enrich(SERVICE2_URL)
		            	.enrich(SERVICE3_URL)
		            	.end()
	            	.log("reponse received :\n ${body}");
	
	            	
	        	from("direct:ADDAnimal")
	        		.setHeader("Accept",constant("application/xml"))
	            	.setHeader(Exchange.HTTP_METHOD,constant("POST"))
	            	.setHeader(Exchange.HTTP_PATH,simple("animals"))
	            	.setHeader(Exchange.CONTENT_TYPE, constant("application/xml"))
	            	.setBody(simple("${property.animal}"))
	            	.multicast(new CenterAggregationStrategy()).parallelProcessing()
	            		.enrich(SERVICE1_URL)
	            		.enrich(SERVICE2_URL)
	            		.enrich(SERVICE3_URL)
	            		.end()
	            	.log("reponse received :\n ${body}");
	        	
	        	from("direct:FindById")
	        	.setHeader("Accept",constant("application/xml"))
	        	.setHeader(Exchange.HTTP_METHOD,constant("GET"))
	        	.setHeader(Exchange.HTTP_PATH,simple("/animals/${property.id}"))
	            .multicast(new AnimalAggregationStrategy()).parallelProcessing()
		           	.enrich(SERVICE1_URL)
		           	.enrich(SERVICE2_URL)
		           	.enrich(SERVICE3_URL)
		           	.end()    
	        	.log("reponse received :\n ${body}");
	        	
	        	from("direct:FindByName")
	            	.setHeader("Accept",constant("application/xml"))
	            	.setHeader(Exchange.HTTP_METHOD,constant("GET"))
	            	.setHeader(Exchange.HTTP_PATH,simple("find/byName/${property.name}"))
		            .multicast(new AnimalAggregationStrategy()).parallelProcessing()
			           	.enrich(SERVICE1_URL)
			           	.enrich(SERVICE2_URL)
			           	.enrich(SERVICE3_URL)
			           	.end()    
	            	.log("reponse received :\n ${body}");
	
	        	
	        	from("direct:GeoNames")
	        		.setHeader("Accept",constant("application/xml"))
	            	.setHeader(Exchange.HTTP_METHOD,constant("GET"))
	            	.setHeader(Exchange.HTTP_QUERY, simple("q=${property.q}&maxRows=1&username=m1gil"))
	            	.to("http://api.geonames.org/search")
	            	.log("reponse received :\n ${body}");
	        }
	    };
	    // On ajoute la route au contexte
	    try {
			routeBuilder.addRoutesToCamelContext(context);
			context.start();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    producerTemplate = context.createProducerTemplate();
    }
    
    
    // -----------------------------------------------------------------------------------------------------------------
    // /animals
    @RequestMapping(path = "/animals", method = GET, produces = APPLICATION_JSON_VALUE)
    public Center getAnimals(){
        return (Center) producerTemplate.sendBody("direct:GETAnimals", ExchangePattern.InOut, "");
    }

    @RequestMapping(path = "/animals", method = POST, produces = APPLICATION_JSON_VALUE)
    public Center addAnimal(@RequestBody Animal animal) throws CageNotFoundException, CamelExecutionException, JAXBException {
    	return (Center) producerTemplate.sendBodyAndProperty("direct:ADDAnimal", ExchangePattern.InOut, "", "animal", marshal(animal));
    }

    // -----------------------------------------------------------------------------------------------------------------
    // /animals/{id}
    @RequestMapping(path = "/animals/{id}", method = GET, produces = APPLICATION_JSON_VALUE)
    public Animal getAnimalById(@PathVariable UUID id) throws AnimalNotFoundException {
    	return (Animal) producerTemplate.sendBodyAndProperty("direct:FindById", ExchangePattern.InOut, "", "id", id.toString());

    }
    
    // -----------------------------------------------------------------------------------------------------------------
    // /find/byName/{name}
    @RequestMapping(path = "/find/byName/{name}", method = GET, produces = APPLICATION_JSON_VALUE)
    public Animal findByName(@PathVariable String name) throws JAXBException {
    	return (Animal) producerTemplate.sendBodyAndProperty("direct:FindByName", ExchangePattern.InOut, "", "name", name);
	}
    
    /**
     * Marshaller d'Animal
     * @param animal
     * @return animal marshalled
     * @throws JAXBException
     */
	private static String marshal(Animal animal) throws JAXBException {
	    StringWriter stringWriter = new StringWriter();

	    JAXBContext jaxbContext = JAXBContext.newInstance(Animal.class);
	    Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

	    jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,
	        true);

	    QName qName = new QName("", "animal");
	    JAXBElement<Animal> root = new JAXBElement<Animal>(qName, Animal.class, animal);

	    jaxbMarshaller.marshal(root, stringWriter);

	    return stringWriter.toString();
	}
}