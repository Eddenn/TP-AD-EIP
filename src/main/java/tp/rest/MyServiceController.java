package tp.rest;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.UUID;

import javax.xml.bind.JAXBException;
import javax.xml.ws.http.HTTPException;

import org.apache.log4j.BasicConfigurator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

import tp.model.*;

import static org.springframework.http.MediaType.*;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@EnableAutoConfiguration
public class MyServiceController {

	/**
	 * Méthode lançeant les trois serveurs
	 * @param args
	 * @throws Exception
	 */
    public static void main(String[] args) throws Exception {
    	BasicConfigurator.configure();
         
        System.getProperties().put( "server.port", "8081" );   
        SpringApplication.run(MyServiceController.class); 
   
        System.getProperties().put( "server.port", "8082" );   
        SpringApplication.run(MyServiceController.class);
        
        System.getProperties().put( "server.port", "8083" );   
        SpringApplication.run(MyServiceController.class);
        
    }

    
    private Center center;

    /**
     * Constructeur initilialisant le centre
     */
    public MyServiceController() {
        // Creates a new center
        center = new Center(new LinkedList<>(), new Position(49.30494d, 1.2170602d), "Biotropica");
        // And fill it with some animals
        Cage usa = new Cage(
                "usa",
                new Position(49.305d, 1.2157357d),
                25,
                new LinkedList<>(Arrays.asList(
                        new Animal("Tic", "usa", "Chipmunk", UUID.randomUUID()),
                        new Animal("Tac", "usa", "Chipmunk", UUID.randomUUID())
                ))
        );

        Cage amazon = new Cage(
                "amazon",
                new Position(49.305142d, 1.2154067d),
                15,
                new LinkedList<>(Arrays.asList(
                        new Animal("Canine", "amazon", "Piranha", UUID.randomUUID()),
                        new Animal("Incisive", "amazon", "Piranha", UUID.randomUUID()),
                        new Animal("Molaire", "amazon", "Piranha", UUID.randomUUID()),
                        new Animal("De lait", "amazon", "Piranha", UUID.randomUUID())
                ))
        );

        center.getCages().addAll(Arrays.asList(usa, amazon));
    }

    // -----------------------------------------------------------------------------------------------------------------
    // /animals
    @RequestMapping(path = "/animals", method = GET, produces = APPLICATION_XML_VALUE)
    public Center getAnimals(){
        return center;
    }

    @RequestMapping(path = "/animals", method = POST, produces = APPLICATION_XML_VALUE)
    public Center addAnimal(@RequestBody Animal animal) throws CageNotFoundException {
        boolean success = this.center.getCages()
                .stream()
                .filter(cage -> cage.getName().equals(animal.getCage()))
                .findFirst()
                .orElseThrow(CageNotFoundException::new)
                .getResidents()
                .add(animal);
        if(success)return this.center;
        throw new IllegalStateException("Failing to add the animal while the input was valid and it's cage was existing is not suppose to happen.");
    }

    // -----------------------------------------------------------------------------------------------------------------
    // /animals/{id}
    @RequestMapping(path = "/animals/{id}", method = GET, produces = APPLICATION_XML_VALUE)
    public Animal getAnimalById(@PathVariable UUID id) throws AnimalNotFoundException {
        return center.findAnimalById(id);
    }
    
    // -----------------------------------------------------------------------------------------------------------------
    // /find/byName/{name}
    @RequestMapping(path = "/find/byName/{name}", method = GET, produces = APPLICATION_XML_VALUE)
    public Animal findByName(@PathVariable String name) throws JAXBException {
		//Search in all cage, all residents
		for( Cage c : this.center.getCages() ) {
			for( Animal a : c.getResidents() ) {
				//Return the animal who have the same name as {name}
				if( a.getName().equals(name) ) {
		    		return a;
				}
			}
		}
		//If no animal found 404 ERROR
		throw new HTTPException(404);
	}
   
}