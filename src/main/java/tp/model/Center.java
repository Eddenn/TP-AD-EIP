package tp.model;

import java.util.Collection;
import java.util.LinkedList;
import java.util.UUID;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Center {

    Collection<Cage> cages;
    Position position;
    String name;

    public Center() {
        cages = new LinkedList<>();
    }

    public Center(Collection<Cage> cages, Position position, String name) {
        this.cages = cages;
        this.position = position;
        this.name = name;
    }

    public Animal findAnimalById(UUID uuid) throws AnimalNotFoundException {
        return this.cages.stream()
                .map(Cage::getResidents)
                .flatMap(Collection::stream)
                .filter(animal -> uuid.equals(animal.getId()))
                .findFirst()
                .orElseThrow(AnimalNotFoundException::new);
    }

    public Collection<Cage> getCages() {
        return cages;
    }

    public Position getPosition() {
        return position;
    }

    @Override
    public String toString() {
        return "Center{" +
                "cages=" + cages +
                ", position=" + position +
                ", name='" + name + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setCages(Collection<Cage> cages) {
        this.cages = cages;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public void aggregate(Center c) {
    	if(c == null || c.getCages() == null) {
    		return;
    	}
    	Boolean cageExist = false;
    	for (Cage cage : c.getCages()) {
    		for (Cage cage2 : this.getCages()) {
    			if (cage2.equals(cage)) {
    				cageExist = true;
    				for (Animal a: cage.getResidents()) {
    					if (!cage2.getResidents().contains(a)) {
    						cage2.getResidents().add(a);
    					}
    				}
    			}
    		}
    		if (!cageExist) {
    			this.getCages().add(cage);
    			cageExist = false;
    		}
    	}
    }
}
