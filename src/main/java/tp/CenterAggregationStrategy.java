package tp;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.transform.stream.StreamSource;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

import tp.model.Center;

/**
 * Classe définissant la stratégie d'agrégation des centres (fusion)
 */
public class CenterAggregationStrategy implements AggregationStrategy {

	@Override
	/**
	 * Fusionne les centres contenu dans les objets Exchange
	 */
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {		
		try {			
			Center c;
			if(newExchange.getIn().getBody(Center.class) != null) {
				c = newExchange.getIn().getBody(Center.class);
			} else {
				c = unmarshalCenter(newExchange.getIn().getBody(String.class));
			}
			Center center = null;
	        if (oldExchange == null) {
	        	center = c;
	        	newExchange.getIn().setBody(c);
	        	return newExchange;
	        } else {
				if(oldExchange.getIn().getBody(Center.class) != null) {
					center = oldExchange.getIn().getBody(Center.class);
				} else {
					center = unmarshalCenter(oldExchange.getIn().getBody(String.class));
				}
				center.aggregate(c);
	        	return oldExchange;
	        }
		
		} catch (JAXBException e) {
			e.printStackTrace();
			return newExchange;
		}
	}

	/**
	 * Unmarshaller de centre
	 * @param xml 
	 * @return centre
	 * @throws JAXBException
	 */
    private Center unmarshalCenter(String xml) throws JAXBException {
    	JAXBContext jc = JAXBContext.newInstance(Center.class);
        return (Center) jc.createUnmarshaller().unmarshal(new StreamSource(new StringReader(xml)));
    }
}