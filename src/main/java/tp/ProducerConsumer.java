package tp;

import java.util.Scanner;

import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.log4j.BasicConfigurator;
 
public class ProducerConsumer {
    public static void main(String[] args) throws Exception {
        // Configure le logger par défaut
        BasicConfigurator.configure();
        // Contexte Camel par défaut
        CamelContext context = new DefaultCamelContext();
        // Crée une route contenant le consommateur
        RouteBuilder routeBuilder = new RouteBuilder() {
            @Override
            public void configure() throws Exception {
            	from("direct:consumer-all")
	            	.choice()
	            		.when(header("destinataire").isEqualTo("écrire"))
	            			.to("direct:consumer-2")
	            		.otherwise()
	            			.to("direct:consumer-1");
            	
            	
                // On définit un consommateur 'consumer-1'
                // qui va écrire le message
                from("direct:consumer-1").to("log:affiche-1-log");
                from("direct:consumer-2").to("file:messages");
            }
        };
        // On ajoute la route au contexte
        routeBuilder.addRoutesToCamelContext(context);
        // On démarre le contexte pour activer les routes
        context.start();
        // On crée un producteur
        ProducerTemplate pt = context.createProducerTemplate();
        // qui envoie un message au consommateur 'consumer-1'
        pt.sendBody("direct:consumer-1", "Hello world !");
        
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        while (!str.equals("exit")) {
        	if(str.startsWith("w")) {
        		pt.sendBodyAndHeader("direct:consumer-all", str, "destinataire", "écrire");
        	} else {
        		pt.sendBody("direct:consumer-all", str);
        	}
        	str = sc.nextLine();
        }
        sc.close();
    }
}