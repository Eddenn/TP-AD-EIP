package tp;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.transform.stream.StreamSource;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

import tp.model.Animal;

/**
 * Classe définissant la stratégie d'agrégation des animaux
 */
public class AnimalAggregationStrategy implements AggregationStrategy {

	@Override
	/**
	 * Choisis un des animaux
	 */
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {		
		try {
			if (oldExchange != null) {
				return oldExchange;
			}
			
			Animal a;
			if(newExchange.getIn().getBody(Animal.class) != null) {
				a = newExchange.getIn().getBody(Animal.class);
			} else {
				a = unmarshalAnimal(newExchange.getIn().getBody(String.class));
			}
	        newExchange.getIn().setBody(a);
	        return newExchange;
		
		} catch (JAXBException e) {
			e.printStackTrace();
			return newExchange;
		}
	}

	/**
	 * Unmarshaller d'animal
	 * @param xml
	 * @return animal
	 * @throws JAXBException
	 */
    private Animal unmarshalAnimal(String xml) throws JAXBException {
    	JAXBContext jc = JAXBContext.newInstance(Animal.class);
        return (Animal) jc.createUnmarshaller().unmarshal(new StreamSource(new StringReader(xml)));
    }
}